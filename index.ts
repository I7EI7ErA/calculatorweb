let button = document.querySelectorAll('.button');
let value1;
let operation;

/*for (var i = 0; i < button.length; i++) { // I don't know why this doesn't work :(
    let value1;
    button[i]?.addEventListener('click', () => {
        let input = document.querySelector('.value');
        console.log(button[i].textContent);
    })
}*/

button[0]?.addEventListener('click', () => { // also don't know why  'as HTMLInputElement' doesn't compile 
    let input = document.querySelector('.value'); // as HTMLInputElement | null;
    input.value = '';
});

button[1]?.addEventListener('click', () => {
    let input = document.querySelector('.value'); 
    if (input?.value)
        input.value = input.value.slice(0, input.value.length - 1);
});

button[2]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[2].textContent;
});

button[3]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[3].textContent;
});

button[4]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[4].textContent;
});

button[5]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    operation = '/';
    if (value1) {
        if (input.value != '0')
            input.value = value1 / input.value;
        else input.value = 'can\'t divide by zero';
        value1 = '';
    }
    else {
        value1 = input.value;
        input.value = '';
    }
});

button[6]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[6].textContent;
});

button[7]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[7].textContent;
});

button[8]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[8].textContent;
});

button[9]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    operation = '*';
    if (value1) {
        input.value = value1 * input.value;
        value1 = '';
    }
    else {
        value1 = input.value;
        input.value = '';
    }
});

button[10]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[10].textContent;
});

button[11]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[11].textContent;
});

button[12]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[12].textContent;
});

button[13]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    operation = '-';
    if (value1) {
        input.value = value1 - input.value;
        value1 = '';
    }
    else {
        value1 = input.value;
        input.value = '';
    }
});

button[14]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    input.value += button[14].textContent;
});

function isContains(str, symb) { // didn't find contain func, wrote my own but types doesn't compile, don't know why T_T
    for (var i = 0; i < str.length; i++)
        if (str[i] == symb) return true;
    return false;
}

button[15]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    if (!isContains(input.value, ',')) input.value += button[15].textContent;
});

button[16]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    operation = '+';
    if (value1) {
        input.value = parseInt(value1) + parseInt(input.value);
        value1 = '';
    }
    else {
        value1 = input.value;
        input.value = '';
    }
});

button[17]?.addEventListener('click', () => {
    let input = document.querySelector('.value');
    switch (operation) {
        case '+':
            input.value = parseInt(value1) + parseInt(input.value); 
            value1 = '';
            break;
        case '-':
            input.value = value1 - input.value;
            value1 = '';
            break;
        case '*':
            input.value = value1 * input.value;
            value1 = '';
            break;
        case '/':
            if (value1) {
                if (input.value != '0') {
                    input.value = value1 / input.value;
                    value1 = '';
                }
                else input.value = 'can\'t divide by zero';
            }
            break;
    }
});